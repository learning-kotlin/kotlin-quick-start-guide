package org.elu.learn.kotlin.quickstart.ch05

fun runLists() {
    val superHeroes = listOf("Batman", "Superman", "Hulk")
    println("list $superHeroes")

    val moreSuperHeroes = arrayListOf("Captain America", "Spider-Man")
    println("arraylist $moreSuperHeroes")

    val superHeroes2 = mutableListOf("Thor", "Daredevil", "Iron Man")
    println("mutablelist before $superHeroes2")

    superHeroes2.add("Wolverine")
    println("mutablelist after $superHeroes2")

    val empty = emptyList<String>()
    println("empty list = $empty")
}
