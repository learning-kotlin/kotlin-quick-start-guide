package org.elu.learn.kotlin.quickstart.ch05

import java.util.*

fun runStandardFunc() {
    println()

    // Apply
    val stringBuilder = StringBuilder().apply {
        appendln("I'm the first line of the String Builder")
    }
    println("stringBuilder apply: $stringBuilder")

    // Let
    val str: String = StringBuilder().let { sb ->
        sb.appendln("First Line")
        sb.appendln("Second Line")
        sb.appendln("Third Line")
        sb.toString()
    }
    println("string let: $str")

    // With
    val strBuilder = StringBuilder()
    with(strBuilder) {
        appendln("First Line")
        appendln("Second Line")
        appendln("Third Line")
    }
    println("string builder with: $strBuilder")

    val str2: String = with(StringBuilder()) {
        appendln("First Line")
        appendln("Second Line")
        appendln("Third Line")
        toString()
    }
    println("string with: $str2")

    // Use
    var inputStream = "Kotlin".byteInputStream()
    val result = inputStream.use { stream ->
        val scanner = Scanner(stream)
        if (scanner.hasNext()) scanner.next() else ""
    }
    println("result use: $result")
    println("input stream: ${inputStream.available()}")

    inputStream = "Kotlin".byteInputStream()
    try {
        inputStream.use {
            error("Reading streams is not supported")
        }
    } catch (ex: Exception) {
        println("input stream: ${inputStream.available()}")
    }
}
