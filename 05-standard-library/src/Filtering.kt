package org.elu.learn.kotlin.quickstart.ch05

fun runFiltering() {
    println()

    // Drop
    val numbers = listOf(1, 2, 3, 4, 5)
    println("numbers: $numbers")

    val dropped = numbers.drop(2)
    println("dropped: $dropped")

    // Filter
    val smallerThan3 = numbers.filter { n -> n < 3 }
    println("smaller than 3: $smallerThan3")

    // FilterNot
    val largerThan3 = numbers.filterNot { n -> n < 3 }
    println("larger than 3: $largerThan3")

    // Take
    val first2 = numbers.take(2)
    println("first 2: $first2")
}
