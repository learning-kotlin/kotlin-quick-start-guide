package org.elu.learn.kotlin.quickstart.ch05

fun runMaps() {
    println()

    val map = mapOf(1 to "one", 2 to "two")
    println("map: $map")

    val hashMap = hashMapOf(3 to "three", 4 to "four")
    println("hashMap: $hashMap")

    val linkedMap = linkedMapOf(5 to "five", 6 to "six")
    println("linkedMap: $linkedMap")

    val sortedMap = sortedMapOf(7 to "seven", 8 to "eight")
    println("sortedMap: $sortedMap")

    val empty = emptyMap<Int, String>()
    println("empty map: $empty")

    val mutableMap = mutableMapOf(9 to "nine", 10 to "ten")
    println("mutable map before: $mutableMap")
    mutableMap.put(5, "five")
    println("mutable map after: $mutableMap")
}
