package org.elu.learn.kotlin.quickstart.ch05

fun main(args: Array<String>) {
    // Collections
    // Lists
    runLists()
    // Sets
    runSets()
    // Maps
    runMaps()
    // indexing
    runIndexing()
    // working with collections
    // filtering functions
    runFiltering()
    // general functions
    runGeneralFunc()
    // transforming functions
    runTransforming()
    // items functions
    runItemsFunc()
    // sorting functions
    runSorting()
    // Kotlin standard library functions
    // Contracts
    runContracts()
    // Standard functions
    runStandardFunc()
}
