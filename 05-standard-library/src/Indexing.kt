package org.elu.learn.kotlin.quickstart.ch05

fun runIndexing() {
    println()

    val superHeroes = mutableListOf("Superman", "Batman")
    println("super heroes: $superHeroes")

    val superman = superHeroes[0]
    println("superman $superman")

    superHeroes += "Thor"
    println("super heroes after: $superHeroes")

    val map = mutableMapOf(1 to "one", 2 to "two")
    println("map: $map")
    val two = map[2]
    println("two: $two")

    map[3] = "three"
    println("map after: $map")
}
