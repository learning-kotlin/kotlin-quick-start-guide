package org.elu.learn.kotlin.quickstart.ch05

fun runSorting() {
    println()
    val numbers = listOf(2, 1, 5, 3, 4)
    println("initial collection: $numbers")

    // Sorted
    val sorted = numbers.sorted()
    println("sorted: $sorted")

    // Reversed
    val reversed = sorted.reversed()
    println("reversed: $reversed")

    // SortedDescending
    val sortedDesc = numbers.sortedDescending()
    println("sorted descending: $sortedDesc")

    // chaining functions
    val result: List<String> = numbers.asSequence() // converts list to sequence to improve performance
        .filter { n -> n % 2 == 0 }
        .map { n -> n * n }
        .sorted()
        .takeWhile { n -> n < 1000 }
        .map { n -> n.toString() }.toList()
    println("chain result: $result")
}
