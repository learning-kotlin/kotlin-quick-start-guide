package org.elu.learn.kotlin.quickstart.ch05

fun runSets() {
    println()

    val superHeroes = setOf("Batman", "Superman")
    println("superHeroes: $superHeroes")

    val superHeroes2 = hashSetOf("Thor", "Spider-Man")
    println("superHeroes2: $superHeroes2")

    val superHeroes3 = linkedSetOf("Iron-Man", "Wolverine")
    println("superHeroes3: $superHeroes3")

    val sortedNums = sortedSetOf(3,10,12,4,9)
    println("sortedNums: $sortedNums")

    val empty = emptySet<String>()
    println("empty set: $empty")

    val mutableSet = mutableSetOf("Hulk", "DareDevil")
    println("mutable set before: $mutableSet")
    mutableSet.add("Wolverine")
    println("mutable set after: $mutableSet")
}