package org.elu.learn.kotlin.quickstart.ch05

fun runTransforming() {
    println()

    val numbers = listOf(1, 2, 3, 4, 5)
    // Map
    val strings = numbers.map { n -> n.toString() }
    println("strings: $strings")

    // FlatMap
    val multiplesOf10 = numbers.flatMap { n -> listOf(n, n * 10) }
    println("multiples of 10: $multiplesOf10")

    // GroupBy
    val strings2 = listOf("abc", "ade", "bce", "bef")
    println("strings: $strings2")
    val grouped = strings2.groupBy { s -> s[0] }
    println("grouped: $grouped")

    // AssociateBy
    val associated = numbers.associateBy { n -> n.toString() }
    println("associated: $associated")
}
