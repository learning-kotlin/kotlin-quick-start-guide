package org.elu.learn.kotlin.quickstart.ch05

fun runItemsFunc() {
    println()

    val numbers = listOf(1, 2, 3, 4, 5)

    // Contains
    val contains10 = numbers.contains(10)
    println("contains 10: $contains10")

    // First
    val firstEven = numbers.first { n -> n % 2 == 0 }
    println("first even: $firstEven")
    try {
        numbers.first { n -> n > 10 }
    } catch (ex: Exception) {
        println("first not found: $ex")
    }

    // FirstOrNull
    val firstLargerThan10 = numbers.firstOrNull { n -> n > 10 }
    println("first larger than 10: $firstLargerThan10")

    // Last
    val lastEven = numbers.last { n -> n % 2 == 0 }
    println("last even: $lastEven")
    try {
        numbers.last { n -> n > 10 }
    } catch (ex: Exception) {
        println("last not found: $ex")
    }

    // LastOrNull
    val lastLargerThan10 = numbers.last { n -> n < 10 }
    println("last larger than 10: $lastLargerThan10")

    // Single
    val number5 = numbers.single { n -> n > 4 }
    println("single number 5: $number5")
    try {
        numbers.single { n -> n > 10 }
    } catch (ex: Exception) {
        println("no single found: $ex")
    }

    // SingleOrNull
    val smallerThan1 = numbers.singleOrNull { n -> n < 1 }
    println("single smaller than1: $smallerThan1")
}
