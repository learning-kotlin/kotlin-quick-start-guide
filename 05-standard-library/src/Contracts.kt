package org.elu.learn.kotlin.quickstart.ch05

fun runContracts() {
    println()

    // Check
    doCheck("abc")
    doCheck("abcdef")
    println()

    // CheckNotNull
    doCheckNotNull(null)
    doCheckNotNull("abc")
    println()

    // Require
    doRequire("abc")
    doRequire("abcdef")
    println()

    // RequireNotNull
    doRequireNotNull(null)
    doRequireNotNull("abc")
    println()

    // Error
    try {
        error("Something went wrong")
    } catch (ex: Exception) {
        println("error was thrown: ${ex.localizedMessage}")
        println(ex)
    }
}

fun doCheck(str: String) {
    try {
        check(str.length > 5) { "Minimum length is 5" }
        println("string [$str] passed check")
    } catch (ex: Exception) {
        println("string [$str] failed check: ${ex.localizedMessage}")
        println(ex)
    }
}

fun doCheckNotNull(str: String?) {
    try {
        val strNoNull: String = checkNotNull(str) { "str argument can not be null" }
        println("[$str] passed check not null: $strNoNull")
    } catch (ex: Exception) {
        println("[$str] failed check not null: ${ex.localizedMessage}")
        println(ex)
    }
}

fun doRequire(str: String) {
    try {
        require(str.length > 5) { "Minimum length is 5" }
        println("[$str] passed require")
    } catch (ex: Exception)  {
        println("[$str] failed require: ${ex.localizedMessage}")
        println(ex)
    }
}

fun doRequireNotNull(str: String?) {
    try {
        val strNoNull: String = requireNotNull(str) { "str argument cannot be null" }
        println("[$str] passed require not null: $strNoNull")
    } catch (ex: Exception) {
        println("[$str] failed require not null: ${ex.localizedMessage}")
        println(ex)
    }
}
