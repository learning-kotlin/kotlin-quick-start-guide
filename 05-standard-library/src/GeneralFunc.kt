package org.elu.learn.kotlin.quickstart.ch05

fun runGeneralFunc() {
    println()

    val numbers = listOf(1, 2, 3, 4, 5)
    println("numbers: $numbers")
    // Any
    val hasEvens = numbers.any { n -> n % 2 == 0 }
    println("has evens: $hasEvens")

    // All
    val allEven = numbers.all { n -> n % 2 == 0 }
    println("all even: $allEven")

    // Count
    val evenCount = numbers.count { n -> n % 2 == 0 }
    println("even count: $evenCount")

    // ForEach
    numbers.forEach { n -> println(n) }

    // Max
    val max = numbers.max()
    println("max: $max")

    // Min
    val min = numbers.min()
    println("min: $min")

    // Sum
    val sum = numbers.sum()
    println("sum: $sum")
}
