package main

fun nullableTypes() {
    println("=====")
    // the following line will cause compilation error
    // val error: String = null
    val nullable: String? = null
    println(nullable)

    nullCheck("hello")
    nullCheck(null)

    safeCall("world")
    safeCall(null)

    safeCallChaining("edu")
    safeCallChaining("")
    safeCallChaining(null)

    nonNullAssertion("finn")
    // next line throws kotlin.KotlinNullPointerException
    // nonNullAssertion(null)

    elvisOperator("not-null")
    elvisOperator(null)

    chainingElvisOperator("eka", "toka")
    chainingElvisOperator(null, "toka")
    chainingElvisOperator("eka", null)
    chainingElvisOperator(null, null)
}

fun nullCheck(str: String?) {
    val upperCase: String? = if (str != null) {
        str.toUpperCase()
    } else {
        null
    }
    println(upperCase)
}

fun safeCall(str: String?) {
    val upperCase: String? = str?.toUpperCase()
    println(upperCase)
}

fun safeCallChaining(str: String?) {
    val firstLetterCapitalized: String? = str?.take(1)?.toUpperCase()
    println("Original string: '$str', converted: $firstLetterCapitalized")
}

fun nonNullAssertion(str: String?) {
    val nonNullString = str!!
    val upperCase: String = nonNullString.toUpperCase()
    println(upperCase)
}

fun elvisOperator(str: String?) {
    val upperCase = str?.toUpperCase() ?: ""
    println("Original string: '$str', converted: '$upperCase'")
}

fun chainingElvisOperator(first: String?, second: String?) {
    val upperCase = first?.toUpperCase() ?: second?.toUpperCase() ?: ""
    println("Original strings: '$first' & '$second', converted: '$upperCase'")
}
