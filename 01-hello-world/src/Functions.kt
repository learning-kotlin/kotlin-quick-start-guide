package main

fun add(a: Int, b: Int): Int {
    return a + b
}

fun fileLevelFunction() {
    println("I'm declared without a class")
}

fun addAsExpression(a: Int, b: Int): Int = a + b

fun printToConsole(input: String): Unit {
    println(input)
}

fun printToConsole2(input: String) {
    println(input)
}

fun testAllFunctions() {
    val result: Int = add(1, 1)
    println(result)

    fileLevelFunction()

    val result1 = addAsExpression(1, 2)
    println(result1)

    printToConsole("Hello")
    printToConsole2("Hello 2")
}