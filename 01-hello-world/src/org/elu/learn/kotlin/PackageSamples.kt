package org.elu.learn.kotlin

import java.lang.Math.PI

fun circumference(radius: Double): Double {
    return 2 * PI * radius
}
