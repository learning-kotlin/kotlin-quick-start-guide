package main

import org.elu.learn.kotlin.circumference

fun main(args: Array<String>) {
    testAllFunctions()

    // local functions
    printUserDetails(User("Edu", "Finn", "Kotikatu"))

    // Basic types
    allTypes()

    // Arrays
    runArrays()

    // Nullable types
    nullableTypes()

    // casting
    runCasting()

    // Control flow
    controlFlow()

    // Exceptions
    runExceptions()

    // Equality comparison
    checkEquality()

    // Packages
    val circ = circumference(10.0)
    println(circ)

    // Visibility
    accessPrivateFunc()
}
