package main

fun immutable() {
    val bar: String = "Kotlin"
    // bar = "Kotlin is awesome" // compiler error
    println(bar)
}

fun mutable() {
    var bar: String = "Kotlin"
    bar = "Kotlin is awesome"
    println(bar)
}

fun example() {
    val bar: String
    if (true) {
        bar = "Foo"
    } else {
        bar = "Bar"
    }
    println(bar)
}
