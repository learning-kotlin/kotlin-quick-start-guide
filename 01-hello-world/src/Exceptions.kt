package main

import java.io.File
import java.io.FileNotFoundException
import java.io.InputStream
import java.lang.IllegalArgumentException

fun runExceptions() {
    println("=====")
    readingFile()

    checkExceptions()
}

fun checkExceptions() {
    try {
        checkNumber(11)
    } catch (ex: IllegalArgumentException) {
        println(ex.localizedMessage)
    }
    try {
        checkDivide(10, 2)
        checkDivide(10, 0)
    } catch (ex: IllegalArgumentException) {
        println(ex.localizedMessage)
    }
}

fun checkNumber(num: Int) {
    if (num !in 1..10) throw IllegalArgumentException("Number has to be from 1 to 10")
}

fun checkDivide(num: Int, divisor: Int) {
    val divide = if (divisor != 0) {
        num / divisor
    } else {
        throw IllegalArgumentException("Can't divide with 0")
    }
    println("Division result = $divide")
}

fun readingFile() {
    val file = File("foo")
    var stream: InputStream? = null
    try {
        stream = file.inputStream()
        // do something with the stream
    } catch (ex: FileNotFoundException) {
        println("File doesn't exist")
    } finally {
        stream?.close()
    }
}
