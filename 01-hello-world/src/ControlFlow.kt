package main

fun controlFlow() {
    println("=====")
    // if-else
    ifElseFun(1)
    ifElseFun(10)

    println("=====")
    // when
    whenFun()

    println("=====")
    // loops
    // for loops
    forLoopsFun()

    // while loops
    whileLoopsFun()

    // do-while loops
    doWhileLoopsFun()
}

fun doWhileLoopsFun() {
    do {
        println("This will be printed only once")
    } while (false)
}

fun whileLoopsFun() {
    var i = 0
    while (i < 10) {
        println("value is $i")
        i += 1
    }
}

fun forLoopsFun() {
    for (i: Int in 0..10) {
        print("$i ")
    }
    println()

    if (5 in 1..10) println("5 found in range")

    for (i in 10..20) {
        print("$i ")
    }
    println()

    val array = arrayOf(1, 2, 3)
    for (i in array) print("$i ")
    println()

    for (i in 1 until 5) {
        print("$i ")
    }
    println()

    downToLoop()
}

fun downToLoop() {
    for (i in 5 downTo 1) {
        print("$i ")
    }
    println()
}

fun whenFun() {
    checkNumbers(1)
    checkNumbers(5)
    checkNumbers(10) // this doesn't do anything

    println(checkNumbersExhaustive(1))
    println(checkNumbersExhaustive(5))
    println(checkNumbersExhaustive(10)) // fall to last else

    whenAny(123)
    whenAny(123.56)
    whenAny("abc")

    whenWithoutArgument(1, 2)
    whenWithoutArgument(20, 6)
    whenWithoutArgument(100, 2)
}

fun checkNumbers(num: Int) {
    when (num) {
        1 -> println("Number is 1")
        2,3,4,5 -> println("Number is in range from 2 to 5")
    }
}

fun checkNumbersExhaustive(num: Int): String {
    return when(num) {
        1 -> "Number is 1"
        2,3,4,5 -> "Number is in range from 2 to 5"
        else -> "Number is higher than 5"
    }
}

fun whenAny(any: Any) {
    when (any) {
        is Int -> println("This is an Int type")
        is Double -> println("This is a Double type")
        is String -> println("This is a String type")
    }
}

fun whenWithoutArgument(a: Int, b: Int) {
    when {
        a + b > 100 -> println("sum of a and b is more than 100")
        a * b > 100 -> println("product of a and b is more than 100")
        a < b -> println("a is less than b")
    }
}

fun ifElseFun(num: Int) {
    val str: String = if (num < 10) "Lower than 10" else "Equal or greater than 10"
    println(str)
}
