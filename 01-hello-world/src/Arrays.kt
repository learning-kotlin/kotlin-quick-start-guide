package main

fun runArrays() {
    println("=====")
    val intArray = arrayOf(1, 2, 3)
    println(intArray.contentToString())

    val squares = IntArray(10) { i -> (i + 1) * (i + 2)}
    println(squares.contentToString())
}
