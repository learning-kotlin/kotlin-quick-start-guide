package main

fun allTypes() {
    println("=====")
    runNumbers()
    println("=====")
    convertNumbers()
    println("=====")
    runBoolean()
    println("=====")
    runChar()
    println("=====")
    runStrings()
}

data class Person(val name: String, val birthYear: Int)

fun runStrings() {
    val s: String = "Kotlin"
    println("String: $s")

    val person = Person("Edu Finn", 1968)
    println("${person.name} was born in ${person.birthYear} year")

    val multiLine: String = """
Hello Kotlin!
Nice to meet you!
"""
    println(multiLine)
}

fun runChar() {
    val a: Char = 'a'
    println("Char: $a")

    val num: Int = 'a'.toInt()
    println("Int from Char: $num")
}

fun runBoolean() {
    var booleanTrue: Boolean = true
    val booleanFalse: Boolean = false

    println("booleanTrue: $booleanTrue")
    println("booleanFalse: $booleanFalse")

    booleanTrue = 1 < 100
    println("booleanTrue: $booleanTrue")
}

fun convertNumbers() {
    val int = 1
    val float = int.toFloat()
    val double = int.toDouble()
    val long = int.toLong()
    val byte = int.toByte()
    val short = int.toShort()

    println("Float: $float")
    println("Double: $double")
    println("Long: $long")
    println("Byte: $byte")
    println("Short: $short")
}

fun runNumbers() {
    val long: Long = 9999L
    val int: Int = 999
    val short: Short = 99
    val byte: Byte = 9

    val float: Float = 9.99f
    val double: Double = 9.9999

    val hex: Int = 0XFF
    val binary: Int = 0b1001001

    println("Long: $long")
    println("Int: $int")
    println("Short: $short")
    println("Byte: $byte")

    println("Float: $float")
    println("Double: $double")

    println("Hex: $hex")
    println("Binary: $binary")
}
