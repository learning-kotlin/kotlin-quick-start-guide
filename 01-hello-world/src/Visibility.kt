package main

private fun sayMyName(name: String) {
    println(name)
}

fun accessPrivateFunc() {
    sayMyName("Edu")
}
