package main

fun runCasting() {
    casting(123)
    // next line causes java.lang.ClassCastException
    // casting("123")

    safeCasting(123)
    safeCasting("123")

    safeCastingWithElvis(123)
    safeCastingWithElvis("123")
}

fun casting(any: Any) {
    val num = any as Int
    println(num)
}

fun safeCasting(any: Any) {
    val num: Int? = any as? Int
    println(num)
}

fun safeCastingWithElvis(any: Any) {
    val num: Int = any as? Int ?: 0
    println(num)
}
