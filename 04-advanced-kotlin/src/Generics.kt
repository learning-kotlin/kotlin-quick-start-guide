package main

fun runGenerics() {
    println("=====")
    val strings: List<String> = ArrayList<String>()
    // omitting generic type from constructor
    val strings1: List<String> = ArrayList()
    // type inference
    val strings2 = ArrayList<String>()

    val nonGeneric = NonGeneric()
    // calling generic function with type
    nonGeneric.genericFunc<String>("Kotlin")
    // same with type inference
    nonGeneric.genericFunc("Kotlin again")
}

interface Generic<T> {
    fun foo(t: T)
}

class IntGeneric : Generic<Int> {
    override fun foo(t: Int) {
        //
    }
}

class GenericType<T> : Generic<T> {
    override fun foo(t: T) {
        //
    }
}

class NonGeneric {
    fun <T> genericFunc(t: T) {
        println(t.toString())
    }
}

// Generic constraints
interface Countable<T : Number> {
    fun count(): T
}

class IntCountable : Countable<Int> {
    override fun count(): Int {
        return 0
    }
}

//compiler error
/*
class StringCountable: Countable<String> {
    override fun count(): String {
        return ""
    }
}
*/
// declaring constraints with were keyword
interface Countable2<T> where T : Number {
    fun count(): T
}

// it is useful for defining multiple constraints on single type
interface Countable3<T> where T : Number, T : Comparable<T> {
    fun count(): T
}

// Reified generics
/*
fun <T> createInstance(): T {
    //compiler error
    return T::class.java.newInstance()
}
*/

inline fun <reified T> createReifiedInstance(): T {
    return T::class.java.newInstance()
}
