package main

import kotlin.properties.Delegates

fun runDelegatedProperties() {
    // lazy
    val str by lazy { "I'm lazily initialized" }
    println(str)

    // observable
    var observable by Delegates.observable(1) { prop, oldVal, newVal ->
        println("Observable property changed from $oldVal to $newVal")
    }
    //this prints Observable property changed from 1 to 0
    observable = 0
    println(observable)

    // vetoable
    var vetoable by Delegates.vetoable(1) { prop, oldVal, newVal ->
        return@vetoable newVal <= 10
    }
    println(vetoable)
    //change is blocked
    vetoable = 100
    println(vetoable)
    //changed to 0
    vetoable = 0
    println(vetoable)

    // notNull
    var notNull by Delegates.notNull<Int>()
    //this results in exception
//    val n = notNull
    notNull = 1
    val num = notNull
    println(notNull)
    println(num)
}
