package main

import kotlin.concurrent.thread
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun runConcurrency() {
    thread {
        println("Work done")
    }
}

@Synchronized
fun threadSafeFunction() {
}

class SampleClass {
    fun synchronizedBlock() {
        // multiple threads can be here
        synchronized(this) {
            //only one thread at a time can be here
        }
    }

    @Volatile
    private var counter = 0

    // Delegated properties
    // private val str by lazyProperty { "I'm lazily initialized" }
}

class LazyProperty<T>(private val valueFactory: () -> T) : ReadOnlyProperty<Any, T> {
    private var instance: T? = null

    override fun getValue(thisRef: Any, property: KProperty<*>): T {
        if (instance == null) {
            instance = valueFactory()
        }
        return instance!!
    }
}
