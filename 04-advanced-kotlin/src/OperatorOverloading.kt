package main

import java.math.BigInteger

class Length(val value: Double) {
    operator fun plus(other: Length): Length {
        return Length(this.value + other.value)
    }

    operator fun plus(double: Double): Length {
        return Length(this.value + double)
    }

    operator fun plus(str: String): String {
        return "$value $str"
    }
}

public inline operator fun BigInteger.times(other: BigInteger): BigInteger = this.multiply(other)

fun sumLengths() {
    println()
    val l1 = Length(12.0)
    val l2 = Length(23.5)

    val sum = l1 + l2
    println("sum = ${sum.value}")

    val l3 = Length(10.0)
    val sum1 = l3 + 12.0
    println("sum1 = ${sum1.value}")

    val str: String = l1 + "kilometers"
    println("sum with string = $str")

    val bint = BigInteger.TEN * BigInteger.TEN
    println("bigint = $bint")

    /*
    val javaL1 = JavaLength(12.0)
    val javaL2 = JavaLength(7.5)

    val javaSum = javaL1 + javaL2
    println("javaSum = $javaSum.value")
    */

    val n = 8 shr 1
    println("shift right (8 >> 1) = $n")
}
