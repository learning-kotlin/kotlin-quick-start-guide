package main

fun runExtensionFunctions() {
    println("=====")
    val n  = 8
    val isPowerOf2 = n.isPowerOf2()
    println("Is power of 2: $isPowerOf2")

    val m = 16
    val isPowerOfTwo = m.isPowerOfTwo
    println("Is power of 2 as property: $isPowerOfTwo")

    // Receiver functions
    val hello: String.() -> Unit = { print("Hello $this") }
    hello("Kotlin")
    println()

    val string = buildString {
        append("Hello Receiver Functions")
        appendln("We have access to StringBuilder object inside this lambda")
    }
    println(string)

    // infix functions
    val employee = Employee()
    employee payout 3500
}

fun Int.isPowerOf2(): Boolean {
    return this > 0 && ((this  and this - 1) == 0)
}

val Int.isPowerOfTwo: Boolean
    get() = this > 0 && ((this and this - 1) == 0)

fun buildString(init: StringBuilder.() -> Unit): String {
    val builder = StringBuilder()
    init(builder)
    return builder.toString()
}

class Employee {
    infix fun payout(salary: Int) {
        print("Employee was paid: $salary")
        println()
    }
}
