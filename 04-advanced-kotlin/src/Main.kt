package main

fun main(args: Array<String>) {
    // Generics
    runGenerics()
    // Concurrency
    runConcurrency()
    // Delegated properties
    runDelegatedProperties()
    // Extension functions
    runExtensionFunctions()
    // DSL
    runDSL()
    // Operator overloading
    sumLengths()
}
