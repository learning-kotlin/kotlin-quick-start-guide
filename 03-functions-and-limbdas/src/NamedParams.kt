package main

fun runNamedParams() {
    println("=====")

    val str = "foo"
    val match = str.regionMatches(thisOffset = 0, other = "foo", otherOffset = 0, length = 3, ignoreCase = true)

    //compiler error
    // val match = str.regionMatches(3, "foo", otherOffset = 0, 3, true)

    val match2 = str.regionMatches(3, "foo", otherOffset = 0, length = 3, ignoreCase = true)

    // named arguments in class constructor
    val user = User(birthYear = 1955, lastName = "Wayne", firstName = "Bruce")
    println("${user.firstName} ${user.lastName} ${user.birthYear}")
}

class User(val firstName: String,
           val lastName: String,
           val birthYear: Int)
