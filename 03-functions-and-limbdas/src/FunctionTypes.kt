package main

import java.util.function.IntPredicate

fun runFunctionTypes() {
    println("=====")

    // Defining function types
    println(multiplier(2, 3))
    print1()
    print2()

    nullableFun?.invoke()
    println(nullableReturnType())

    // calling with named parameters
    longRunningTask3({ result -> println("Result: $result") }, { fail -> println("Error: ${fail.message}") })

    longRunningTask3({ r -> println("Result: $r") }, { err -> println("Error: ${err.message}") })

    // Lambdas and SAM types
    println(createPredicate())

    // Member references
    val numFunc: (Int, Int) -> Int = { a, b -> multiplier(a, b) }
    println(numFunc(3, 4))

    val numFunc2: (Int, Int) -> Int = ::multiplier2
    println(numFunc2(4, 5))

    val str = "Kotlin"
    val getChar: (Int) -> Char = str::get
    println(getChar(0))

    class Person(val name: String)
    val personConstructor = ::Person
    val person: Person = personConstructor("John")
    println(person.name)

    // Inlining lambdas
    noInline { "no inlinde Kotlin" }
    inlined { "inlined Kotlin" }

    println("str1 = $str1")
    str1 = "Kotlin as well"
    println("str1 = $str1")

    println("str2 = $str2")
    str2 = "Kotlin again"
    println("str2 = $str2")

    println(str3)
    str3 = "Kotlin"
}

val multiplier: (Int, Int) -> Int = { a, b -> a * b }

val print1: () -> Unit = { println("Kotlin") }
// following is same
val print2 = { println("Kotlin Again") }

val nullableFun: (() -> Unit)? = null

val nullableReturnType: () -> String? = { null }

// Calling function types
fun longRunningTask1(onFinished: (Any) -> Unit, onError: (Throwable) -> Unit) {
    try {
        //something long running
        onFinished("got result")
    } catch (fail: Throwable) {
        onError(fail)
    }
}

fun longRunningTask2(onFinished: (result: Any) -> Unit, onError: (fail: Throwable) -> Unit) {
    try {
        //something long running
        onFinished.invoke("got result")
    } catch (fail: Throwable)
    {
        onError.invoke(fail)
    }
}

// Naming parameters of function types
fun longRunningTask3(onFinished: (result: Any) -> Unit, onError: (fail: Throwable) -> Unit) {
    try {
        //something long running
        onFinished("got result")
    } catch (fail: Throwable) {
        onError(fail)
    }
}

// Lambdas and SAM types
fun createPredicate(): IntPredicate {
    return IntPredicate { n -> n % 2 == 0 }
}

// Member references
fun multiplier2(a: Int, b: Int): Int {
    return a * b
}

// Inlining lambdas
fun noInline(func: () -> String) {
    val str = func()
    println("Func produced: $str")
}

inline fun inlined(func: () -> String) {
    val str = func()
    println("Func produced: $str")
}

var str1: String
    inline get() = "Kotlin"
    inline set(value) {
        println("$value passed as parameter")
    }

var str2: String
    get() = "Kotlin"
    inline set(value) {
        println("$value passed as parameter")
    }

inline var str3: String
    get() = "Kotlin"
    set(value) {
        println("$value passed as parameter")
    }

// noinline
inline fun noInlineExample(first: () -> String, noinline second: () -> String) {
    //omitted
}

// crossinline
fun firstFunctionType(f: () -> String) {
    println(f.invoke())
}
inline fun secondFunctionType(crossinline f: () -> String) {
    firstFunctionType {
        f.invoke()
    }
}