package main

fun runDefaultParameters() {
    println("=====")
    indexOf(65)
    indexOf(65, 10)

    val customer = createCustomer("Finn Customer")
    println(customer)
}

fun indexOf(ch: Int, fromIndex: Int = 0) {
    println("Character $ch from index $fromIndex")
}

class VerboseCustomer {
    constructor(name: String) : this(name, null)

    constructor(name:String,
                phoneNumber: String?) : this(name, phoneNumber, false)

    constructor(name:String,
                phoneNumber: String?,
                isLoggedIn: Boolean) {
        //omitted
    }
}

// same less verbose
class Customer(name: String,
               phoneNumber: String? = null,
               isLoggedIn: Boolean = false)

// for Java interop
@JvmOverloads
fun createCustomer(name: String, phoneNumber: String = "", loggedIn: Boolean = false) : Customer {
    return Customer(name, phoneNumber, loggedIn)
}
