package main

fun runLambdas() {
    println("=====")
    lambdaExample1()

    val result = ({ a: Int, b: Int -> a + b })(3, 4)
    println(result)
    // compiler error; lambda have implicit return
    // ({ a: Int, b: Int -> return a + b })

    // closure
    var a = 0
    val closure = {
        a++
    }
    println("a = $a")
    closure()
    println("a = $a")

    // returning from lambdas
    returnStatement1()
    returnStatement2()
    returnStatement3()
}

fun lambdaExample1() {
    val result = ({ a: Int, b: Int ->
        println("Summing numbers: $a and $b")
        val result = a + b
        println("The result is: $result")
        result
    })(1, 2)
    println("example 1 result $result")
}

fun returnStatement1() {
    val nums = arrayOf(1, 2, 3, 4, 5)
    println("Started iterating the array")
    nums.forEach { n ->
        if (n > 2) {
            return
        }
    }
    println("Finished iterating the array")
}

fun returnStatement2() {
    val nums = arrayOf(1, 2, 3, 4, 5)
    println("Started iterating the array")
    nums.forEach { n ->
        if (n > 2) {
            return@forEach
        }
    }
    println("Finished iterating the array")
}

fun returnStatement3() {
    val lamdba = label@ {
        return@label
    }
}
