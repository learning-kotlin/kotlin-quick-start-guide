package main

fun runVariableArguments() {
    println("=====")
    val result = sumNumbers(1, 2, 3, 4, 5)
    println(result)

    val nums = intArrayOf(1, 2, 3, 4, 5)
    val result1 = sumNumbers(*nums)
    println(result1)
}

fun sumNumbers(vararg num: Int): Int {
    var result = 0
    for (i in num) {
        result += i
    }
    return result
}
