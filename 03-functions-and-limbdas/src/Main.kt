package main

fun main(args: Array<String>) {
    // Functions
    // Named parameters
    runNamedParams()

    // Default parameters
    runDefaultParameters()

    // Variable function arguments
    runVariableArguments()

    // Lambdas
    runLambdas()

    // Function types
    runFunctionTypes()
}