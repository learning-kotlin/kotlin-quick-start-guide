package main

fun runEnums() {
    println("=====")

    val march = Month.MARCH
    println(march)

    val may = Month2.MAY
    val mayNum = may.num
    println(may)
    println(mayNum)

    for (month in Month.values()) {
        print("$month ")
    }
    println()

    val june = Month.valueOf("JUNE")
    println(june)
}

enum class Month {
    JANUARY,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
}

enum class Month2 (val num: Int) {
    JANUARY(1),
    FEBRUARY(2),
    MARCH(3),
    APRIL(4),
    MAY(5),
    JUNE(6),
    JULY(7),
    AUGUST(8),
    SEPTEMBER(9),
    OCTOBER(10),
    NOVEMBER(11),
    DECEMBER(12)
}
