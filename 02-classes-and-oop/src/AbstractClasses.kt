package main

fun runAbstractClasses() {
    println("=====")
    val user = AnotherUser("Edu Finn")
    println("User: ${user.name}")
    user.login()
    user.logout()
}

abstract class BaseUser {
    abstract val name: String
    abstract fun login()

    //compiler error
    // abstract val name: String = ""

    //compiler error
    /* abstract fun login() {
        //login the user
    } */

    fun logout(){
        println("Logging out")
    }
}

class AnotherUser(override val name: String) : BaseUser() {
    override fun login(){
        println("Logging in")
    }
}