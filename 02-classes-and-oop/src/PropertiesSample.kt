package main

class PropertiesSample {
    private var _name: String = ""
    var name: String = _name
        get() {
            println("name property is being accessed")
            return _name
        }

    private var _firstName: String = ""
    var firstName: String = _firstName
        set(value) {
            println("first name property is being set with value: $value")
            _firstName = value
        }

    var lastName: String = ""
        get() {
            println("last name property is being accessed")
            return field
        }
        set(value) {
            println("last name property is being set with value: $value")
            field = value
        }

    private var _year: String = ""
    var year: String //doesn't have to be initialized
        get() {
            println("year property is being accessed")
            return _year
        }
        set(value) {
            println("year property is being set with value: $value")
            _year = value
        }

    var eka: String = ""
        private set

    private lateinit var toka: String
}
