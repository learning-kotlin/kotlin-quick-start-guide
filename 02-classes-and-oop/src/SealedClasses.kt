package main

fun runSealedClasses() {
    println("=====")
    val hulk = Hulk()
    val spiderMan = SpiderMan()
    val superMan = SuperMan()

    actOnHero(hulk)
    actOnHero(spiderMan)
    actOnHero(superMan)
}

sealed class SuperHero

class Hulk: SuperHero() {
    fun smashOpponent() {
        println("Smashing opponents")
    }
}
class SuperMan : SuperHero() {
    fun flyToKrypton() {
        println("Flying to Krypton")
    }
}
class SpiderMan : SuperHero() {
    fun useSpiderSense() {
        println("Using spider sense")
    }
}
fun actOnHero(hero: SuperHero) {
    when (hero) {
        is Hulk -> {
            hero.smashOpponent()
        }
        is SuperMan -> {
            hero.flyToKrypton()
        }
        is SpiderMan -> {
            hero.useSpiderSense()
        }
    }
}
