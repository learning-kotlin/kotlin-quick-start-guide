package main

fun runDataClasses() {
    println("=====")

    val user = DataUser("Bruce", "Wayne", "1965")
    println("before copy: $user")

    val userCopy = user.copy(firstName = "John")
    println("after copy: $user")
    println("copy: $userCopy")
}

data class DataUser(var firstName: String,
                    var lastName: String,
                    var birthYear: String)
