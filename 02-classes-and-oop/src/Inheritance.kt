package main

fun runInheritance() {
    println("=====")

    val admin = AdminUser("Eka", "Admin", "1960", "admin")
    println("${admin.firstName} ${admin.lastName} ${admin.birthYear} ${admin.role}")

    val adminOne = AdminUserOne("Eka", "Admin", "1960", "admin")
    println(adminOne)
}

open class MyUser(var firstName: String,
                  var lastName: String,
                  var birthYear: String)

class AdminUser(firstName: String,
                lastName: String,
                birthYear: String,
                val role: String): MyUser(firstName, lastName, birthYear)

open class MyUserOne {
    constructor(firstName: String,
                lastName: String,
                birthYear: String)
}

class AdminUserOne : MyUserOne {
    constructor(
        firstName: String,
        lastName: String,
        birthYear: String,
        role: String) : super(firstName, lastName, birthYear)
}