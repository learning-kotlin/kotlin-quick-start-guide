package main

fun runSecondaryConstructors() {
    println("====")
    val eka = EkaUser("Eka", "Finn", 1940)
    println(eka)

    val toka = TokaUser("Eka", "Finn", 1940)
    println(toka)

    val kolmas = KolmasUser("Kolmas", "Finn", 1950)
    println(kolmas)
}

class EkaUser {
    constructor(firstName: String,
                lastName: String,
                birthYear: Int)
}

class TokaUser(firstName: String,
               lastName: String) {
    constructor(firstName: String,
                lastName: String,
                birthYear: Int): this(firstName, lastName)
}

class KolmasUser {
    constructor(firstName: String,
                lastName: String) {}

    constructor(firstName: String,
                lastName: String,
                birthYear: Int) {}
}

fun runPrimaryConstructors() {
    println("====")
    val privateProperty = PrivateProperty(5)
//    println(privateProperty.num) // num cannot be accessed

    val ekaUser = UserEka("Eka", "Finn", 1910)
    println("${ekaUser.firstName} ${ekaUser.lastName} ${ekaUser.birthYear}")

    val tokaUser = UserToka("Toka", "Finn", 1920)
    println("${tokaUser.firstName} ${tokaUser.lastName} ${tokaUser.birthYear}")

    val kolmasUser = UserKolmas("Kolmas", "Finn", 1930)
    println("${kolmasUser.firstName} ${kolmasUser.lastName} ${kolmasUser.birthYear}")

    MultipleInits()
}

class PrivateProperty(private val num: Int)

class UserEka(firstName: String,
              lastName: String,
              birthYear: Int) {
    var firstName: String
    var lastName: String
    var birthYear: Int
    init {
        println("Calling constructor on UserEka class")
        this.firstName = firstName
        this.lastName = lastName
        this.birthYear = birthYear
    }
}

// previous user class is same as this
class UserToka(firstName: String,
               lastName: String,
               birthYear: Int) {
    var firstName: String = firstName
    var lastName: String = lastName
    var birthYear: Int = birthYear
}

// previous user class is same as this
class UserKolmas(var firstName: String,
                 var lastName: String,
                 var birthYear: Int)

class MultipleInits {
    private var counter = 1

    init {
        //called first
        println("I'm called $counter time(s)")
    }

    init {
        //called second
        println("I'm called $counter time(s)")
    }
}
