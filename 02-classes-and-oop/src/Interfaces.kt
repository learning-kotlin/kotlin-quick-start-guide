package main

fun runInterfaces() {
    println("=====")
    val car = Car()
    car.drive()

    val plane = AirPlane()
    plane.climb()
    plane.fly()

    val drone = Drone()
    drone.climb()
    drone.fly()

    val horse = Horse("Speedy")
    println("Horse: ${horse.name}")
    horse.ride()
}

interface Drivable {
    fun drive()
}

class Car : Drivable {
    override fun drive() {
        println("Driving a car")
    }
}

interface Flyable {
    fun climb() {
        println("Climbing")
    }

    fun fly()
}

class AirPlane : Flyable {
    override fun fly() {
        println("Flying a plane")
    }
}

class Drone : Flyable {
    override fun climb() {
        println("Climbing slowly")
    }

    override fun fly() {
        println("Flying a drone")
    }
}

interface Ridable {
    fun ride()
    val name: String
}

class Horse(override val name: String) : Ridable {
    override fun ride() {
        println("Riding a horse")
    }
}
