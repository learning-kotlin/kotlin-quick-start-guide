package main

fun runOverridingMembers() {
    println("=====")
    val base = Base()
    base.print()

    val derived = Derived()
    derived.print()

    val derivedTwo = DerivedTwo()
    derivedTwo.print()
}

open class Base {
    open fun print() {
        println("I'm called from the base type")
    }
}

class Derived : Base() {
    // by default overridden member is open
    override fun print() {
        println("I'm called from the super type")
    }
}

open class DerivedTwo: Base() {
    // closing this member for overriding
    final override fun print() {
        println("I'm called from the super type two")
    }
}

open class UserTwo(open val name: String)
class AdminTwo(override var name: String): UserTwo(name)
