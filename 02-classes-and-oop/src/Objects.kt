package main

import java.io.Writer
import java.util.*

fun runObjects() {
    println("=====")

    // Singleton
    //compiler error
//    val singleton = Singleton()
    Singleton.sayMyName()

    RunnableSingleton.run()

    println(SingletonWithInitializer.name)

    // Companion objects
    println("====")
    val userFromEmail = UserNelos.newUserWithEmail("john@mail.com")
    println(userFromEmail.userId)
    val userFromUUID = UserNelos.newUserFromUUID(UUID.randomUUID())
    println(userFromUUID.userId)

    Outer.saySomething()

    // Static with companion objects
    println("====")
    Static.staticFunction()
    println(Static.staticField)

    // Anonymous objects
    println("====")
    // anonymous objects from interfaces
    Thread(object : Runnable {
        override fun run() {
            println("I'm created with anonymous object")
        }
    }).run()

    val runnable = object : Runnable {
        override fun run() {
            println("I'm created with anonymous object two")
        }
    }
    runnable.run()

    // this is same as above
    val runnable2 = Runnable { println("I'm created with anonymous object three") }
    runnable2.run()

    // anonymous objects from classes
    val writer = object : Writer() {

        override fun write(cbuf: CharArray, off: Int, len: Int) {
            // implementation omitted
        }

        override fun flush() {
            // implementation omitted
        }

        override fun close() {
            // implementation omitted
        }
    }
}

class Static {
    companion object {

        // static method
        @JvmStatic
        fun staticFunction() {
            println("this is static function")
        }

        // static field
        @JvmField
        val staticField = 0
    }
}

class UserNelos private constructor(val userId: String) {
    companion object {
        fun newUserWithEmail(email: String): UserNelos
        {

            return UserNelos(email)

        }

        fun newUserFromUUID(uuid: UUID): UserNelos {
            return UserNelos(uuid.toString())
        }
    }
}

class Outer {
    companion object Inner {
        fun saySomething() {
            println("Hello")
        }
    }
}

object Singleton {
    fun sayMyName() {
        println("I'm a singleton")
    }
}

object RunnableSingleton : Runnable {
    override fun run() {
        println("I'm a runnable singleton")
    }
}

object SingletonWithInitializer {
    var name = ""

    init {
        name = "Singleton"
    }
}
