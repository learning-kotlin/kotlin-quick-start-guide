package main

fun runClassDelegation() {
    println("=====")
    // delegation with interfaces
    val car = CarOne()
    val vehicle = Vehicle(car)
    vehicle.drive()

    // delegation with classes
    val eBike = ChargableBike(Bike(), Battery())
    eBike.charge()
    eBike.ride()
}

interface DrivableOne {
    fun drive()
}

class CarOne : DrivableOne {
    override fun drive() {
        println("Driving a car")
    }
}

class Vehicle(car: CarOne) : DrivableOne by car

interface Rideable {
    fun ride()
}

interface Chargable {
    fun charge()
}

class Battery : Chargable {
    override fun charge() {
        println("Charging")
    }
}

class Bike : Rideable {
    override fun ride() {
        println("Riding a bike")
    }
}

class ChargableBike(bike: Bike, battery: Battery): Rideable by bike, Chargable by battery
