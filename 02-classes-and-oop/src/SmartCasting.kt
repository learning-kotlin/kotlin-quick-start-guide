package main

fun runSmartCasts() {
    println("=====")
    val user = UserLast("Edu User", 1961)
    val admin = AdminUserLast("Edu Admin", 1962)

    smartCasting(user)
    smartCasting(admin)
}

open class BaseUserLast(val name: String)

open class UserLast(name: String,
                    val birthYear: Int) : BaseUserLast(name) {
    fun login() {
        println("Logging in")
    }
}
open class AdminUserLast(name: String,
                         birthYear: Int) : UserLast(name, birthYear) {
    fun accessLogs() {
        println("Accessing logs")
    }
}

fun smartCasting(baseUser: BaseUserLast) {
    if (baseUser is UserLast) {
        baseUser.login()
    } else if (baseUser is AdminUserLast) {
        baseUser.accessLogs()
    }
}
