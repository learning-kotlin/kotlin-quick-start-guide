package main

fun runNestedClasses() {
    println("=====")
    val address = NelosUser.Address("Kotikatu", "1", "00900", "Helsinki")
    println("${address.street} ${address.streetNumber}, ${address.zip} ${address.city}")

    val user = SeiskaUser("Seiska")
    val otherAddress = user.Address("Main", "123", "ABC", "City")
    println("${otherAddress.street} ${otherAddress.streetNumber}, ${otherAddress.zip} ${otherAddress.city}")
}

class NelosUser(val name: String) {
    class Address(val street: String,
                  val streetNumber: String,
                  val zip: String,
                  val city: String)
}

class VitosUser(val name: String) {
    // private nested class
    private class Address(val street: String,
                  val streetNumber: String,
                  val zip: String,
                  val city: String)
    // private inner variable
    private val address = Address("Kotikatu", "10", "00901", "Helsinki")
}

class KutosUser(val name: String) {
    // nested class cannot access outer class's members
    class Address(val street: String,
                  val streetNumber: String,
                  val zip: String,
                  val city: String) {
        init {
            // compiler error, cannot access name property
            // println("The name of the User is: $name")
        }
    }
}

class SeiskaUser(val name: String) {
    // inner class has access to outer class members
    inner class Address(val street: String,
                  val streetNumber: String,
                  val zip: String,
                  val city: String) {
        init {
            println("The name of the User is: $name")
        }
    }
}
