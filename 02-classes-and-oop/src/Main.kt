package main

fun main(args: Array<String>) {
    // Classes
    val user = User("Edu", "Finn", "1900")
    println("${user.firstName} ${user.lastName} ${user.birthYear}")

    // Properties
    val props = PropertiesSample()
    props.name = "edu" // this statement does not have an effect
    println(props.name)
    props.firstName = "Edu"
    println(props.firstName) // this statement does not have an effect
    props.lastName = "Finn"
    println(props.lastName)
    props.year = "1900"
    println(props.year)
//    props.eka = "Eka" // causes compilation error
    println(props.eka)

    // Constructors
    // Primary constructors
    runPrimaryConstructors()
    // Secondary constructors
    runSecondaryConstructors()

    // Nested classes
    runNestedClasses()

    // Enum classes
    runEnums()

    // Data classes
    runDataClasses()

    // Inheritance
    runInheritance()

    // Overriding members
    runOverridingMembers()

    // Abstract classes
    runAbstractClasses()

    // Interfaces
    runInterfaces()

    // Object keyword
    runObjects()

    // Class delegation
    runClassDelegation()

    // Sealed classes
    runSealedClasses()

    // Smart casts
    runSmartCasts()
}
